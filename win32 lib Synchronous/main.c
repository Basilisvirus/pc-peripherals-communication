//https://github.com/rrmhearts/Serial-Programming-Win32API-C
//https://www.codeproject.com/Articles/2682/Serial-Communication-in-Windows
	//====================================================================================================//
	// Serial Port Programming using Win32 API in C                                                       //
	// (Writes data to serial port)                                                                      //
	//====================================================================================================//

	//====================================================================================================//
	// www.xanthium.in										                                              //
	// Copyright (C) 2014 Rahul.S                                                                         //
	//====================================================================================================//

	//====================================================================================================//
	// The Program runs on the PC side and uses Win32 API to communicate with the serial port or          //
	// USB2SERIAL board and writes the data to it.                                                       //
	//----------------------------------------------------------------------------------------------------//
	// Program runs on the PC side (Windows) and transmits a single character.                            //
	// Program uses CreateFile() function to open a connection serial port(COMxx).                        //
	// Program then sets the parameters of Serial Comm like Baudrate,Parity,Stop bits in the DCB struct.  //
	// After setting the Time outs,the Program writes a character to COMxx using WriteFile().             //
    //----------------------------------------------------------------------------------------------------//
	// BaudRate     -> 9600                                                                               //
	// Data formt   -> 8 databits,No parity,1 Stop bit (8N1)                                              //
	// Flow Control -> None                                                                               //
	//====================================================================================================//


	//====================================================================================================//
	// Compiler/IDE  :	Microsoft Visual Studio Express 2013 for Windows Desktop(Version 12.0)            //
	//               :  gcc 4.8.1 (MinGW)                                                                 //
	// Library       :  Win32 API,windows.h,                                                              //
	// Commands      :  gcc -o USB2SERIAL_Write_W32 USB2SERIAL_Write_W32.c                                //
	// OS            :	Windows(Windows 7)                                                                //
	// Programmer    :	Rahul.S                                                                           //
	// Date	         :	30-November-2014                                                                  //
	//====================================================================================================//

	//====================================================================================================//
	// Sellecting the COM port Number                                                                     //
    //----------------------------------------------------------------------------------------------------//
    // Use "Device Manager" in Windows to find out the COM Port number allotted to USB2SERIAL converter-  //
    // -in your Computer and substitute in the  "ComPortName[]" array.                                    //
	//                                                                                                    //
	// for eg:-                                                                                           //
	// If your COM port number is COM32 in device manager(will change according to system)                //
	// then                                                                                               //
	//			char   ComPortName[] = "\\\\.\\COM32";                                                    //
	//====================================================================================================//

#include <Windows.h>
#include <stdio.h>
#include <string.h>

HANDLE comPortSetup(char ComPrt[])
{
    int CharLoop=0;

    HANDLE HandleCom;                 // Handle to the Serial port

    /*----------------------------------- Opening the Serial Port --------------------------------------------*/

    /*
    There might be a case where one would need to use CreateFileA instead. (Depending on the compiler)
    More can be found here: https://stackoverflow.com/questions/51462048/what-is-the-difference-between-createfile-and-createfilea
    */
    HandleCom = CreateFile( ComPrt,                       // Name of the Port to be Opened
                        GENERIC_READ | GENERIC_WRITE,      // Read/Write Access
                        0,                                 // No Sharing, ports cant be shared
                        0,                              // No Security
                        OPEN_EXISTING,                     // Open existing port only
                        0,                                 // Non Overlapped I/O (0 does not match any of the flags of dwFlagsAndAttributes.
                                                           // This means we are setting no flags or attributes (We dont care about it) https://stackoverflow.com/questions/17997608/what-does-dwflagsandattributes-0-mean-in-the-createfile-method
                        0);                             // Null for Comm Devices

    if (HandleCom == INVALID_HANDLE_VALUE)
        printf("\n   Error! - Port %s can't be opened", ComPrt);
    else
        printf("\n   Port %s Opened\n ", ComPrt);

    /*------------------------------- Setting the Parameters for the SerialPort ------------------------------*/

    DCB dcbSerialParams = { 0 };

                        // Initializing DCB (device control block) structure
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    BOOL StatusFun;

    StatusFun = GetCommState(HandleCom, &dcbSerialParams);     //retreives  the current settings

    if (StatusFun == FALSE)
        printf("\n   Error! in GetCommState()");

    dcbSerialParams.BaudRate = CBR_9600;      // Setting BaudRate = 9600
    dcbSerialParams.ByteSize = 8;             // Setting ByteSize = 8
    dcbSerialParams.StopBits = ONESTOPBIT;    // Setting StopBits = 1
    dcbSerialParams.Parity   = NOPARITY;      // Setting Parity = None

    StatusFun = SetCommState(HandleCom, &dcbSerialParams);  //Configuring the port according to settings in DCB

    if (StatusFun == FALSE)
        {
            printf("\n   Error! in Setting DCB Structure");
        }
    else
        {
            printf("\n   Setting DCB Structure Successfull\n");
            printf("\n       Baudrate = %d", dcbSerialParams.BaudRate);
            printf("\n       ByteSize = %d", dcbSerialParams.ByteSize);
            printf("\n       StopBits = %d", dcbSerialParams.StopBits);
            printf("\n       Parity   = %d", dcbSerialParams.Parity);
        }
    /*------------------------------------ Setting Timeouts --------------------------------------------------*/

    COMMTIMEOUTS timeouts = { 0 };

    //miliseconds (ms) intervals
    //interval between the arrival of any two bytes. Terminates the ReadFile
    timeouts.ReadIntervalTimeout         = 50; //Default =50
    //Total = (TimeoutMultiplier*BytesToRead + TimeoutConstant)
    timeouts.ReadTotalTimeoutConstant    = 50; //Default = 50
    timeouts.ReadTotalTimeoutMultiplier  = 10; //Default = 10
    //Total = (TimeoutMultiplier*BytesToRead + TimeoutConstant)
    timeouts.WriteTotalTimeoutConstant   = 50; //Default = 50
    timeouts.WriteTotalTimeoutMultiplier = 10; //Default = 10

    if (SetCommTimeouts(HandleCom, &timeouts) == FALSE)
        printf("\n   Error! in Setting Time Outs");
    else
        printf("\n\n   Setting Serial Port Timeouts Successfull");

    return HandleCom;
}

BOOL rx_setup(HANDLE HandleCom)
{
/*------------------------------------ Setting Receive Mask ----------------------------------------------*/
    //https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-setcommmask
    BOOL Status;
    Status = SetCommMask(HandleCom, EV_RXCHAR); //Configure Windows to Monitor the serial device for Character Reception

    if (Status == FALSE)
    {
         printf("\n\n    Error! in Setting CommMask");
    }

    /*------------------------------------ Setting WaitComm() Event   ----------------------------------------*/

    printf("\n\n    Waiting for Data Reception...");

    DWORD dwEventMask; // Event mask to trigger. 32-bit unsigned integer (range: 0 through 4294967295 decimal)
    //Wait for the first character to be received
    Status = WaitCommEvent(HandleCom, &dwEventMask, NULL); //dwEventMask Should be 1 for "A character was received and placed in the input buffer." AKA EV_RXCHAR
    /*-------------------------- Program will Wait here till one Character is received ------------------------*/

    if (Status == FALSE)
    {
        printf("\n  Error! in Setting WaitCommEvent()");
        exit(-1);
    }

    return Status;
}

void rx_receive(HANDLE HandleCom, char *SerialBufferFun, int printIt)
{
    char  TempChar;                        // Temperory Character
    BOOL Status;

    /*Receiver start*/
    DWORD NoBytesRead;                     // Bytes read by ReadFile()
    int loopArrayFun = -1;
    do
    {
        Status = ReadFile(HandleCom,
                          &TempChar,
                          sizeof(TempChar),         //No of bytes to be read
                          &NoBytesRead,             //How many bytes were actually read
                          NULL);
        SerialBufferFun[loopArrayFun] = TempChar;
        if(printIt == 1)
        {
             printf("%c",SerialBufferFun[loopArrayFun]);
        }
        loopArrayFun = loopArrayFun+1;
    }while (NoBytesRead > 0);//NoBytesRead = 0 when bytes are finished reading.

SerialBufferFun[loopArrayFun-3] = '\0'; //WHY -3
}

void tx_transmit(HANDLE HandleCom, char DataToTransmit[])
{
    BOOL Status;

        /*----------------------------- Writing a Character to Serial Port----------------------------------------*/
    //DataToTransmit should be  char or byte array, otherwise write will fail
    DWORD  dNoOFBytestoWrite;              // No of bytes to write into the port
    DWORD  dNoOfBytesWritten = 0;          // No of bytes written to the port

    dNoOFBytestoWrite = sizeof(DataToTransmit); // Calculating the no of bytes to write into the port

        Status = WriteFile(HandleCom,               // Handle to the Serialport
                       DataToTransmit,            // Data to be written to the port
                       (dNoOFBytestoWrite),   // No of bytes to write into the port
                       &dNoOfBytesWritten,  // No of bytes written to the port
                       NULL);


    if (Status != TRUE)
        printf("\n\n   Error %d in Writing to Serial Port",GetLastError());
}

int main(void)
{
    //Create Handler
    HANDLE hComm = comPortSetup("\\\\.\\COM26");//change this to the com port of your mcu

    //Setup the Receiver (not needed)
    //rx_setup(hComm);

    sleep(2);
    char tranData[125] = ".";
    tx_transmit(hComm, &tranData);

    char  SerialBuffer[512];

    do
    {
        rx_receive(hComm, SerialBuffer, 1);
        //float temperature = atof(SerialBuffer);
    }
    while(SerialBuffer[0] != '-');

    CloseHandle(hComm);//Closing the Serial Port

    printf("Finished");

    while(1)
    {

    }

    while (1)
    {
        char
         user_input = '\0';

        printf("R for receive, T for transmit: ");
        while(user_input !='R' && user_input != 'T')
        {
            scanf(" %c", &user_input); // Note the leading white-space, it's what does the skipping //wait for the next user input
        }

        if(user_input == 'R')//receive
        {

        }
    }

    CloseHandle(hComm);//Closing the Serial Port
    _getch();//press any key to close the window

}









