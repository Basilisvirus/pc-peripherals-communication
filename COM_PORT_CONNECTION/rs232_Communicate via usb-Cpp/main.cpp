/*
This code is used for ASCII character debugging, not using any kind of
protocol communication or structed text.

There is a .TMS flag that, when sent, it initiates a plotting loop using matplotlib.
In order to fetch data correctly and form the plot, the data sent is filtered
since sent data may be sent half (when buffer is full).

*/



/*
yOU ALSO NEED a working installation of MATPLITLIB FOR PYTHON!!
*/

/*
overcome numpy error
*/
#define WITHOUT_NUMPY
#define _USE_MATH_DEFINES


/*
Check library include settings and linker settings.
These should change accordingly.
*/
#include <Windows.h>


#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

#ifdef _WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#include "rs232.h"

//Matplotlib Includes
/*
You need to download python
After that, install numpy with pip install numpy
If there is a numpy error, use: #define WITHOUT_NUMPY
*/

#include <malloc.h>
#include <memory.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <string.h>
#include <cmath>
#include "matplotlibcpp.h"

namespace plt = matplotlibcpp; // need to install matplotlib (on windows with): python -m pip install -U matplotlib --prefer-binary


using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc

//Matplotlib includes

int cport_nr = 3;/* /dev/ttyS0 (0=COM1 on windows) */


void closeComPortAtexit(void )
{
    RS232_CloseComport(cport_nr);
}

void temperatureGraph();

int main()
{
    /*
    temperature vector to append values and time vector for seconds
    */
    std::vector<double> timeVect;
    std::vector<double> temperatureVect;//initialize the vector (again)
    double seconds=0;//initial time = 0, appends in time vector

    plt::ion();
    plt::show();

    printf("Comport shutdown: Press 's'\n\r ");
    printf("select comport and pres enter: ");

    char user_input[2];
    user_input[0]= '\0';//initialize user input
    user_input[1]= '\0';//initialize user input

    //wait untill user presses enter
    while(user_input[0] == '\0')
    {
        scanf(" %c", &user_input[0]); // Note the leading white-space, it's what does the skipping //wait for the next user input
        scanf(" %c", &user_input[1]);
    }

    cport_nr = atoi(user_input);//convert char to number
    printf("connecting to COM%d \n\r", cport_nr);
    cport_nr = cport_nr - 1;//(0=COM1 on windows)

    int i, n, bdrate=1000000;       /* 38400 baud */
    int prevDataLoop;//here we save previous data.


    //use SetConsoleCtrlHandler  instead?
    atexit(closeComPortAtexit);//close port at exit

    unsigned char buf[4096];
    unsigned char completeLine[4096];
    unsigned char previousData[4096];
    unsigned char matplotlibString[4096];

    char mode[]={'8','N','1',0};


  if(RS232_OpenComport(cport_nr, bdrate, mode, 0))
  {
    printf("Can not open comport\n");
    return(0);
  }

  char keypress;//save the key pressed here
//  int pressed=0;//flag that key is not pressed


    int anyModeIsActive=0;//if any mode is active
    int temperatureMode=0;//if temperature mode is active
    int noEolDetected=0;//this indicates that the last char is not \n
    int completeLineLength=-1;//the length of the final char
    int fixLine=0;//flag to fix line in the next iteration
    int useFixed=0;//flag to indicate wether to use fixed value of not.

    while(1)
    {
        if(!(_kbhit()))//if key is not pressed
        {

        n = RS232_PollComport(cport_nr, buf, 4095);//look for new characters

        if(n > 0)//if character is received
        {
            noEolDetected=0;//reset line error
            buf[n] = 0;   /* always put a "null" at the end of a string! */

        for(i=0; i < n; i++)
        {
            if(buf[i] < 32)  /* replace unreadable control-codes */
            {
                if(buf[i] == 10)//\n
                {
                    buf[i] = '\n';
                }
                else if(buf[i] == 13)//\r
                {
                    buf[i] = '\r';
                }
                else if(buf[i] == 0)
                {
                    //usually this happens in the beggining of a line.
                    buf[i] = ' ';//null
                }
                else//if the error in no one from the above
                {
                    printf(">buf[i]=%d<", buf[i]);//print it so we can resolve it
                }
            }//END if(buf[i] < 32)
        }//END for(i=0; i < n; i++)


            //the buf[]variable started sometimes with a ' ' gap so we want to fix this
            //but if the buf[0] is really an ' ', then we should not change that.
            if(buf[0] == ' ')
            {
                for(i=0; i<(n); i++)
                {
                    completeLine[i] = buf[i+1];//move the array one block back
                }
                completeLineLength = strlen(reinterpret_cast<char*>(completeLine));
                int bufLength = strlen(reinterpret_cast<char*>(buf));

                //if the new length chat is not 1 less than the previous
                if ((completeLineLength)!= (bufLength-1))
                {
                    printf(">ERROR DIFFERENT BUF SIZES: %d %d<",completeLineLength,bufLength);
                }
            }
            else//if it does not start with a gap
            {
                for(i=0; i<(n+1); i++)
                {
                    completeLine[i] = buf[i];//let the array as it is.
                    matplotlibString[i] = buf[i];//fill the same array for matplotlib
                }
            }

            /*
            Print the new characters here, not below, since the array gets changed.
            */
            printf("%s", (char *)completeLine);//print the new characters

            //if we have a line to fix
            completeLineLength = strlen(reinterpret_cast<char*>(completeLine));
            if(fixLine == 1 && anyModeIsActive > 0)
            {//merge this line with the previous
                int completeLineLoop=0;
               // printf("\nPrevDataLoop=%d\n",prevDataLoop);
                //printf("\ncompleteLineLength=%d\n",completeLineLength);

                for(i=prevDataLoop; i<(completeLineLength+(2*prevDataLoop)); i++)
                { //merging..
                    previousData[i] = completeLine[completeLineLoop];
                    completeLineLoop++;
                }
                //printf("\ncompleteLineLoop=%d\n",completeLineLoop);
                previousData[completeLineLoop]='\0';
                //printf("\n>%s<\n", (char *)previousData);

                //pass all the data to completeLine, since this is the one the
                //matplotlib will use.

                int previousDataLength = strlen(reinterpret_cast<char*>(previousData));
                for(i=0; i<(previousDataLength+1); i++)
                {
                    matplotlibString[i]=previousData[i];
                }
                //printf("\n>%s<\n", (char *)matplotlibString);
                useFixed = 1;

                fixLine = 0;//line is fixed.
            }

            /*
            If the last char is \n, this means we read up to the end of a line.
            if it is not, this means the data read is either not complete
            or its not a series of input data.
            */
            if(completeLine[completeLineLength-1] == '\n' && anyModeIsActive > 0)
            {
                noEolDetected=0;//line is complete
            }
            else
            {
                noEolDetected=1;//line is not complete
                fixLine=1;//flag to fix the line in the next iteration
                //store this array to merge it with the next one
                //to make a complete array/line of data
                for(prevDataLoop=0; prevDataLoop<(completeLineLength); prevDataLoop++)
                {
                    previousData[prevDataLoop]=completeLine[prevDataLoop];
                }
                previousData[prevDataLoop]='\0';
                //printf("\n>%s<\n", (char *)previousData);//print the new characters");
            }

            double temperatureDouble;

            if(useFixed == 1 && anyModeIsActive > 0)
            {

                //printf("%s,", (char *)matplotlibString);
                temperatureDouble = atof(reinterpret_cast<char*>(matplotlibString));
                useFixed = 0;//reset use fixed
            }
            else if (useFixed == 0 || anyModeIsActive > 0)
            {
                //printf("%s,", (char *)completeLine);
                temperatureDouble = atof(reinterpret_cast<char*>(completeLine));
            }


    /*
    MODES
    */

            /*
            If temperatureDouble == 0, this means usually that the temperaturedouble is wrong
            aka there was text instead of numbers gived to transform to temperature, so check
            your input data.
            */
            if(temperatureMode == 1 && fixLine == 0 && temperatureDouble != 0.00)//if we are in temperature mode
            {
                seconds=seconds+2;//one second passed
                timeVect.push_back(seconds);//new second, append to vector

                printf(">\nTEMP=%f\n<",temperatureDouble);
                temperatureVect.push_back(temperatureDouble);//new temperature, append to vector

                //plt::clf();
                //plt::subplot(1,1,1);
                plt::plot(timeVect, temperatureVect);
                plt::draw();

                plt::show();
                plt::pause(0.01);
                //plt::pause(0.001);
            }
            //check if there is a code
            if(temperatureMode == 0//if temperatureMode is not active
             && completeLine[0] == '.'
             && completeLine[1] == 'T'
             && completeLine[2] == 'M'
             && completeLine[3] == 'S')
            {
                printf("->TMS<-");
                temperatureMode = 1;
                anyModeIsActive++;
            }
            else if(completeLine[0] == '-')//end of temperatureMode
            {
                //printf("->PrintMode finished<-");
                temperatureMode = 0;
                anyModeIsActive--;
            }

        }//END if(!(_kbhit()))

    #ifdef _WIN32
        Sleep(100);
    #else
        usleep(100000);  /* sleep for 100 milliSeconds */
    #endif
      }//end if(!(_kbhit()))
      else//key pressed!
      {    //pressed=1;//flag that key is pressed
           keypress=_getch();//get the key pressed

           if(keypress == 's')//shutdown
           {
               printf("Closing com port..\n\r");
               RS232_CloseComport(cport_nr);//close com port
               printf("Comport Closed");
           }
           else if(keypress == ']')//start receiving temperature
           {
                temperatureGraph();
           }
           else if(keypress == 'h')
           {
                printf(">Enter Hex message ex: 2A and press enter: <");
                char userInputnputHex[3];
                userInputnputHex[2]='\0';

                // Note the leading white-space, it's what does the skipping //wait for the next user input
                scanf(" %c%c", &userInputnputHex[0], &userInputnputHex[1]);//read two chars
                uint8_t hexCommand = (int)strtol(userInputnputHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hexCommand);//send the byte
                printf(">Message sent<\n");
           }
           else
           {
               printf("%c",keypress);
               RS232_SendByte(cport_nr, keypress);
           }
      }

    /* if(pressed==1)//if key is pressed
    {
        getch();//wait for a character
        printf("resuming...\n\r");
        pressed=0;//done with key pressing, resetting flag
    }
    */

    }//end while(1)

  return(0);
}


void temperatureGraph()
{

}


/*
int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
*/
