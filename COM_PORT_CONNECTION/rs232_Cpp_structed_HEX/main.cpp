/*
Built in Code::Blocks 20.03
This code is used for ASCII character debugging, not using any kind of
protocol communication or structed text.

There is a .TMS flag that, when sent, it initiates a plotting loop using matplotlib.
In order to fetch dat
a correctly and form the plot, the data sent is filtered
since sent data may be sent half (when buffer is full).

*/

/*
Check library include settings and linker settings.
These should change accordingly.
*/
#include <Windows.h>


#include <stdlib.h>
#include <stdio.h>
#include <conio.h>// for _kbhit

#ifdef _WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#include "rs232.h"

//Matplotlib Includes
/*
You need to download python
After that, install numpy with pip install numpy
If there is a numpy error, use: #define WITHOUT_NUMPY
*/

#include <malloc.h>
#include <memory.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <string.h>


using namespace std;


//Matplotlib includes

int cport_nr = 3;/* /dev/ttyS0 (0=COM1 on windows) */


void closeComPortAtexit(void )
{
    RS232_CloseComport(cport_nr);
}

void temperatureGraph();

int main()
{
    char user_q = '\0';//initialize user input

    printf("Use hex print on this session? (Can be changed later by pressing 'c' (y or n): ");

    while(user_q == '\0')
    {
        scanf(" %c", &user_q); // Note the leading white-space, it's what does the skipping //wait for the next user input
    }

    int useHex;

    if(user_q == 'y')
    {
        useHex = 1;
    }
    else if (user_q == 'n')
    {
        useHex = 0;
    }


    printf(">To shutdown a ComPort: Press 's'\n ");
    printf("To send a hex message: Press 'h'\n");
    printf("select comport and pres enter: <");


    char user_input[2];
    user_input[0]= '\0';//initialize user input
    user_input[1]= '\0';//initialize user input

    //wait untill user presses enter
    while(user_input[0] == '\0')
    {
        scanf(" %c", &user_input[0]); // Note the leading white-space, it's what does the skipping //wait for the next user input
        scanf(" %c", &user_input[1]);
    }

    cport_nr = atoi(user_input);//convert char to number
    printf("connecting to COM%d \n\r", cport_nr);
    cport_nr = cport_nr - 1;//(0=COM1 on windows)


    int i, n, bdrate=9600;       /* 38400 baud */


    //use SetConsoleCtrlHandler  instead?
    atexit(closeComPortAtexit);//close port at exit

    unsigned char buf[4096];//using "unsigned" gets rid of weird print trash values

    char mode[]={'8','N','1',0};


  if(RS232_OpenComport(cport_nr, bdrate, mode, 0))
  {
    printf("Can not open comport\n");
    return(0);
  }

  char keypress;//save the key pressed here
//  int pressed=0;//flag that key is not pressed

    while(1)
    {
        if(!(_kbhit()))//if key is not pressed
        {

        n = RS232_PollComport(cport_nr, buf, 4095);//look for new characters

        if(n > 0)//if character is received
        {
            buf[n] = 0;   /* always put a "null" at the end of a string! */

        for(i=0; i < n; i++)
        {
            if(buf[i] < 32)  /* replace unreadable control-codes */
            {
                {
                   // printf(">(HEX)buf[i]=%X<", buf[i]);//print it so we can resolve it
                }
            }//END if(buf[i] < 32)
        }//END for(i=0; i < n; i++)

            printf("%s,", (char *)buf);//print the new characters

            if(useHex)//if we selected to use hex
            {
                for(i=0; i<n; i++)
                {
                    printf("%X.", buf[i]);
                }
            }
        }

    #ifdef _WIN32
        Sleep(100);
    #else
        usleep(100000);  /* sleep for 100 milliSeconds */
    #endif
      }//end if(!(_kbhit()))
      else//key pressed!
      {
           keypress=_getch();//get the key pressed

           if(keypress == 's')//shutdown
           {
                printf(">Closing com port..\n");
                RS232_CloseComport(cport_nr);//close com port
                printf("Comport Closed<");
           }
           else if(keypress == 'h')//Send hex code
           {
                printf(">Enter Hex message ex: 2A and press enter: <");
                char userInputnputHex[3];
                userInputnputHex[2]='\0';

                // Note the leading white-space, it's what does the skipping //wait for the next user input
                scanf(" %c%c", &userInputnputHex[0], &userInputnputHex[1]);//read two chars
                uint8_t hexCommand = (int)strtol(userInputnputHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hexCommand);//send the byte
                printf(">Message sent<\n");
           }
           else if(keypress == 't')//test
           {

               /*
                uint8_t hex8;

                char OutHex[3];
                OutHex[0]='2';
                OutHex[1]='3';
                OutHex[2]='\0';
                hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hex8);//send the byte

                OutHex[0]='2';
                OutHex[1]='A';
                OutHex[2]='\0';
                hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hex8);//send the byte

                OutHex[0]='0';
                OutHex[1]='0';
                OutHex[2]='\0';
                hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hex8);//send the byte

                OutHex[0]='0';
                OutHex[1]='A';
                OutHex[2]='\0';
                hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
                RS232_SendByte(cport_nr, hex8);//send the byte
                */
                printf("Sending data..");
                for (int hexLoop = 0; hexLoop<250; hexLoop++)
                {
                 RS232_SendByte(cport_nr, 0x20);//send the byte
                }
                printf("finished\n");

           }
           else if (keypress == 'c')
           {
                user_q = '\0';//initialize user input

                printf("Use hex print? (y or n): ");

                while(user_q == '\0')
                {
                    scanf(" %c", &user_q); // Note the leading white-space, it's what does the skipping //wait for the next user input
                }

                if(user_q == 'y')
                {
                    useHex = 1;
                }
                else if (user_q == 'n')
                {
                    useHex = 0;
                }
           }
           else//send the byte typed
           {
               printf("%c",keypress);
               RS232_SendByte(cport_nr, keypress);//send the byte
           }
      }

    }//end while(1)

  return(0);
}

/*
int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
*/
