/**************************************************

file: demo_rx.c
purpose: simple demo that receives characters from
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

compile with the command: gcc demo_rx.c rs232.c -Wall -Wextra -o2 -o test_rx

**************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <stdint.h>

#ifdef _WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#include "rs232.h"


//Matplotlib includes


/* /dev/ttyS0 (0=COM1 on windows) */

void temperatureGraph();

int main()
{


    int i, n, bdrate=38400;       /* 9600 baud */


    unsigned char buf[4096];
    int availableComPorts[256];

    char mode[]={'8','N','1',0};

//just scan the com ports
RS232_OpenComport(1, bdrate, mode, 0,1, availableComPorts);

//availableComPorts[0]=availableComPorts[1];

int availComLoop=0;
while(availableComPorts[availComLoop] != 0)
{

    RS232_OpenComport(availableComPorts[availComLoop]-1, bdrate,mode,0,0,0);
        #ifdef _WIN32
            Sleep(100);
        #else
            usleep(100000);  /* sleep for 100 milliSeconds */
        #endif
    uint16_t loopEnd=0;
    printf("\nTesting: %d\n",availableComPorts[availComLoop]);

    /*
    uint8_t hex8;
    char OutHex[3];

    OutHex[0]='2';
    OutHex[1]='3';
    OutHex[2]='\0';
    hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
    RS232_SendByte(availableComPorts[availComLoop]-1, hex8);//send the byte

    OutHex[0]='2';
    OutHex[1]='0';
    OutHex[2]='\0';
    hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
    RS232_SendByte(availableComPorts[availComLoop]-1, hex8);//send the byte

    OutHex[0]='0';
    OutHex[1]='0';
    OutHex[2]='\0';
    hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
    RS232_SendByte(availableComPorts[availComLoop]-1, hex8);//send the byte

    OutHex[0]='0';
    OutHex[1]='A';
    OutHex[2]='\0';
    hex8 = (int)strtol(OutHex, NULL, 16);//transform it to hex
    RS232_SendByte(availableComPorts[availComLoop]-1, hex8);//send the byte
    */

    uint32_t hex32=0;
    hex32 |= 0x0A << 24;
    hex32 |= 0x00 << 16;
    hex32 |= 0x20 << 8;
    hex32 |= 0x23;
    printf("hex32: %X\n", hex32);

    uint8_t hex8[4];

    hex8[0] = 0x23;
    hex8[1] = 0x20;
    hex8[2] = 0x00;
    hex8[3] = 0x0A;

    RS232_SendBuf(availableComPorts[availComLoop]-1, hex8,4);//send the bytes

    //printf("\n%X.%X.%X.%X\n",hex8[0],hex8[1],hex8[2],hex8[3]);

    printf("Reply: ");
    while(loopEnd < 1000)
    {
        n = RS232_PollComport(availableComPorts[availComLoop]-1, buf, 4095);//look for new characters
        if(n > 0)//if character is received
        {
          buf[n] = 0;   /* always put a "null" at the end of a string! */
          //printf("%X", (char *)buf);//print the new characters
            for(i=0; i<n; i++)
            {
                printf("%X.", buf[i]);
            }
        }

        #ifdef _WIN32
            Sleep(100);
        #else
            usleep(100000);  // sleep for 100 milliSeconds
        #endif



        if(n<=0)
        {
            loopEnd++;
        }
        printf(".");
    }
    RS232_CloseComport(availableComPorts[availComLoop]-1);

    //printf(",%d",availableComPorts[availComLoop]);
    availComLoop++;
}
printf("\nfinished\n");
return 0;

//start connecting and checking
RS232_OpenComport(availableComPorts[1]-1, bdrate, mode,0,0,availableComPorts);


  char keypress;//save the key pressed here
//  int pressed=0;//flag that key is not pressed

  while(1)
  {
      if(!(_kbhit()))//if key is not pressed
      {

        n = RS232_PollComport(availableComPorts[1]-1, buf, 4095);//look for new characters

        if(n > 0)//if character is received
        {
          buf[n] = 0;   /* always put a "null" at the end of a string! */

          for(i=0; i < n; i++)
          {
            if(buf[i] < 32)  /* replace unreadable control-codes */
            {
                if(buf[i] == 10)//\n
                {
                    buf[i] = '\n';
                }
                else if(buf[i] == 13)//\r
                {
                    buf[i] = '\r';
                }
                else if(buf[i] == 0)
                {
                    buf[i] = ' ';//null
                }
                else//if the error in no one from the above
                {
                    printf("buf[i]=%d!", buf[i]);//print it so we can resolve it
                }
            }//if(buf[i] < 32)
          }//for(i=0; i < n; i++)

              printf("%s", (char *)buf);//print the new characters
        }

    #ifdef _WIN32
        Sleep(100);
    #else
        usleep(100000);  /* sleep for 100 milliSeconds */
    #endif
      }//end if(!(_kbhit()))
      else//key pressed!
      {    //pressed=1;//flag that key is pressed
           keypress=_getch();//get the key pressed

           if(keypress == 's')//shutdown
           {
               printf("Closing com port..\n\r");
               RS232_CloseComport(availableComPorts[1]-1);//close com port
               printf("Comport Closed");
           }
           else if(keypress == '.')//start receiving temperature
           {
                temperatureGraph();
           }
           else
           {
               printf("%c",keypress);
               RS232_SendByte(availableComPorts[1]-1, keypress);
           }
      }

   /* if(pressed==1)//if key is pressed
    {
        getch();//wait for a character
        printf("resuming...\n\r");
        pressed=0;//done with key pressing, resetting flag
    }
    */

    }//end while(1)

  return(0);
}


void temperatureGraph()
{

}
