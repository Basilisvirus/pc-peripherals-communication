/**************************************************

file: demo_rx.c
purpose: simple demo that receives characters from
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

compile with the command: gcc demo_rx.c rs232.c -Wall -Wextra -o2 -o test_rx

**************************************************/

#include <stdlib.h>
#include <stdio.h>
//#include <conio.h>

#ifdef _WIN32
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#include "rs232.h"


//Matplotlib includes



int cport_nr = 8;/* /dev/ttyS0 (0=COM1 on windows) */


void closeComPortAtexit(void )
{
    RS232_CloseComport(cport_nr);
}

void temperatureGraph();

int main()
{
    int temperatureMode =0;

    printf("Comport shutdown: Press 's'\n\r ");
    printf("select comport and pres enter: ");

    char user_input[2];
    user_input[0]= '\0';//initialize user input
    user_input[1]= '\0';//initialize user input

    //wait untill user presses enter
    while(user_input[0] == '\0')
    {
        scanf(" %c", &user_input[0]); // Note the leading white-space, it's what does the skipping //wait for the next user input
        scanf(" %c", &user_input[1]);
    }

    cport_nr = atoi(user_input);//convert char to number
    printf("connecting to COM%d \n\r", cport_nr);
    cport_nr = cport_nr - 1;//(0=COM1 on windows)


    int i, n, bdrate=9600;       /* 9600 baud */

    //use SetConsoleCtrlHandler  instead?
    atexit(closeComPortAtexit);//close port at exit

    unsigned char buf[4096];

    char mode[]={'8','N','1',0};


  if(RS232_OpenComport(cport_nr, bdrate, mode, 0))
  {
    printf("Can not open comport\n");

    return(0);
  }

  char keypress;//save the key pressed here
//  int pressed=0;//flag that key is not pressed

  while(1)
  {
      //if(!(_kbhit()))//if key is not pressed
      //{

        n = RS232_PollComport(cport_nr, buf, 4095);//look for new characters

        if(n > 0)//if character is received
        {
          buf[n] = 0;   /* always put a "null" at the end of a string! */

          for(i=0; i < n; i++)
          {
            if(buf[i] < 32)  /* replace unreadable control-codes */
            {
                if(buf[i] == 10)//\n
                {
                    buf[i] = '\n';
                }
                else if(buf[i] == 13)//\r
                {
                    buf[i] = '\r';
                }
                else if(buf[i] == 0)
                {
                    buf[i] = ' ';//null
                }
                else//if the error in no one from the above
                {
                    printf("buf[i]=%d!", buf[i]);//print it so we can resolve it
                }
            }//if(buf[i] < 32)
          }//for(i=0; i < n; i++)
          if(temperatureMode == 1)//if we are in temperature mode
          {

          }
          else//is no mode is defined
          {
              printf("%s", (char *)buf);//print the new characters
          }
        }
        else
        {
            printf(".");
        }

    #ifdef _WIN32
        Sleep(100);
    #else
        usleep(100000);  /* sleep for 100 milliSeconds */
    #endif
      //}//end if(!(_kbhit()))
    /*  else//key pressed!
      {    //pressed=1;//flag that key is pressed
           keypress=_getch();//get the key pressed

           if(keypress == 's')//shutdown
           {
               printf("Closing com port..\n\r");
               RS232_CloseComport(cport_nr);//close com port
               printf("Comport Closed");
           }
           else if(keypress == '.')//start receiving temperature
           {
                temperatureGraph();
           }
           else
           {
               printf("%c",keypress);
               RS232_SendByte(cport_nr, keypress);
           }
      }*/

   /* if(pressed==1)//if key is pressed
    {
        getch();//wait for a character
        printf("resuming...\n\r");
        pressed=0;//done with key pressing, resetting flag
    }
    */

    }//end while(1)

  return(0);
}


void temperatureGraph()
{

}
