#include <iostream>
#include <windows.h>
#include <stdio.h>
#include "ftd2xx.h"
#include <cstdlib>
#include <unistd.h>
//#include <conio.h>//for _kbhit

using namespace std;

int main(int argc, char* argv[])
{

    //================ASk user for Hex or ASCI print
   char user_q = '\0';//initialize user input

    printf("Use hex print on this session? (Can be changed later by pressing 'c' (y or n): ");

    while(user_q == '\0')
    {
        scanf(" %c", &user_q); // Note the leading white-space, it's what does the skipping //wait for the next user input
    }

    int useHex;

    if(user_q == 'y')
    {
        useHex = 1;
    }
    else if (user_q == 'n')
    {
        useHex = 0;
    }

    //============initiate D2XX vars
    FT_STATUS ftStatus;
    DWORD numDevs;

    FT_HANDLE ftHandle;

    DWORD EventDWord;
    DWORD TxBytes;
    DWORD RxBytes;
    DWORD BytesReceived;
    DWORD BytesWritten;


    FT_DEVICE_LIST_INFO_NODE *devInfo;
    // create the device information list
    ftStatus = FT_CreateDeviceInfoList(&numDevs);
    if (ftStatus == FT_OK)
    {
        printf("Number of devices is %d\n",numDevs);
    }
    if (numDevs > 0)
    {
        // allocate storage for list based on numDevs
        devInfo =
        (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
        // get the device information list
        ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
        if (ftStatus == FT_OK)
        {
            for (int i = 0; i < numDevs; i++)
            {
                printf("Dev %d:\n",i);
                printf(" Flags=0x%x\n",devInfo[i].Flags);
                printf(" Type=0x%x\n",devInfo[i].Type);
                printf(" ID=0x%x\n",devInfo[i].ID);
                printf(" LocId=0x%x\n",devInfo[i].LocId);
                printf(" SerialNumber=%s\n",devInfo[i].SerialNumber);
                printf(" Description=%s\n",devInfo[i].Description);
                printf(" ftHandle=0x%x\n",devInfo[i].ftHandle);
            }
        }
    }

    //============Open the first available Device
    ftStatus = FT_Open(0,&ftHandle);
    if (ftStatus == FT_OK)
    {
        // FT_ClrRts OK
        printf("Open ok\n");
    }
    else
    {
        // FT_ClrRts failed
        printf("Open failed\n");
        return 0;
    }

    //===============Set 8 data bits, 1 stop bit and no parity
    ftStatus = FT_SetDataCharacteristics(ftHandle, FT_BITS_8, FT_STOP_BITS_1,
    FT_PARITY_NONE);
    if (ftStatus == FT_OK)
    {
        printf("FT_SetDataCharacteristics OK\n");
    }
    else
    {
        printf("FT_SetDataCharacteristics Failed\n");
    }

    //=================Set Baud Rate
    ftStatus = FT_SetBaudRate(ftHandle, 1000000); // Set baud rate to 1M
    if (ftStatus == FT_OK)
    {
        // FT_ClrRts OK
        printf("BDrate ok\n");
    }
    else
    {
        // FT_ClrRts failed
        printf("BDrate failed\n");
        return 0;
    }

    //=============send reset signal to MCU
    ftStatus = FT_SetRts(ftHandle);
    usleep(100);//100us
    ftStatus = FT_ClrRts(ftHandle);

    //check if Clear suceeded
    if (ftStatus == FT_OK)
    {
        // FT_ClrRts OK
        printf("clr ok\n");
    }
    else
    {
        // FT_ClrRts failed
        printf("clr failed\n");
    }

    if (ftStatus == FT_OK)
    {
        // FT_Open OK, use ftHandle to access device
        printf("Device opened\n");
        unsigned char RxBuffer[256];//using "unsigned" gets rid of weird print trash values
        char keypress;//save the key pressed here
        uint16_t bufferLoop;

        while(1)
        {
           // if(!(_kbhit()))//if key is not pressed
            //{
                FT_GetStatus(ftHandle,&RxBytes,&TxBytes,&EventDWord);
                if (RxBytes > 0)
                {
                    //printf("bytes to read\n");
                    ftStatus = FT_Read(ftHandle,RxBuffer,RxBytes,&BytesReceived);
                    RxBuffer[RxBytes] = '\0';

                    if (ftStatus == FT_OK)
                    {
                        if(useHex)
                        {
                            for(bufferLoop=0; bufferLoop<RxBytes; bufferLoop++)
                            {
                                printf("%X.", RxBuffer[bufferLoop]);
                            }
                        }
                        else if(!useHex)
                        {
                            printf("%s", (char *)RxBuffer);//print the new characters up to \0
                        }
                    }
                    else
                    {
                        // FT_Read Failed
                        printf("read failed\n");
                    }
                }
            //}
            /*else//if key is pressed
            {
                keypress=_getch();//get the key pressed

                if(keypress == 's')//Shutdown the communication
                {
                    FT_Close(ftHandle);
                    printf("device closed\n");
                }
                else if(keypress == 'h')//send hex code
                {
                    printf(">Enter Hex message ex: 2A and press enter: <");
                    char userInputnputHex[3];
                    userInputnputHex[2]='\0';

                    // Note the leading white-space, it's what does the skipping //wait for the next user input
                    scanf(" %c%c", &userInputnputHex[0], &userInputnputHex[1]);//read two chars
                    uint8_t hexCommand = (int)strtol(userInputnputHex, NULL, 16);//transform it to hex
                    ftStatus = FT_Write(ftHandle, &hexCommand, sizeof(hexCommand), &BytesWritten);
                    printf(">Message sent<\n");
                }
                else if (keypress == 'c')//change from hex to ASCII print
                {
                    user_q = '\0';//initialize user input

                    printf("Use hex print? (y or n): ");

                    while(user_q == '\0')
                    {
                        scanf(" %c", &user_q); // Note the leading white-space, it's what does the skipping //wait for the next user input
                    }

                    if(user_q == 'y')
                    {
                        useHex = 1;
                    }
                    else if (user_q == 'n')
                    {
                        useHex = 0;
                    }
                }
                else//send the byte typed
                {
                    printf("%c",keypress);
                    ftStatus = FT_Write(ftHandle, &keypress, sizeof(keypress), &BytesWritten);
                }
           */ }
        }
    }
    else
    {
        // FT_Open failed
        printf("open failed\n");
    }

    FT_Close(ftHandle);
    printf("device closed\n");

	return 0;
}
