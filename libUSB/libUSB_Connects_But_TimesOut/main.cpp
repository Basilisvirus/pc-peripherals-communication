#include <libusb.h>
#include <stdio.h>
libusb_context* context = NULL;


int main(void)
{
    libusb_device_handle* handle = NULL;
    int r = libusb_init(&context); //initialize the library using libusb_init

    if (r != 0)
    {
      fprintf(stderr, "Error initialising libusb.\n");
      return 1;
    }


    libusb_device **devs;

    ssize_t cnt;


    //scan and get a list of connected USB devices to the Host
    cnt = libusb_get_device_list(NULL, &devs);

    if (cnt < 0)
    {
        libusb_exit(NULL);
        return (int) cnt;
    }


    libusb_device *dev;
    int i = 0;
    while ((dev = devs[i++]) != NULL)
    {
        struct libusb_device_descriptor desc;

        int ret;
        unsigned char stringl[256];
        //request a device descriptor
        int r = libusb_get_device_descriptor(dev, &desc);

        if (r < 0)
        {
            fprintf(stderr, "failed to get device descriptor");
            return 0;
        }

        //we need to call libusb_open with that device and get a handle (reference number)
        ret = libusb_open(dev, &handle);

        if (LIBUSB_SUCCESS == ret)
        {
                printf("get %04x:%04x device string descriptor \n", desc.idVendor, desc.idProduct);
                printf("iProduct[%d]:\n", desc.iProduct);
                ret = libusb_get_string_descriptor_ascii(handle, desc.iProduct,stringl, sizeof(stringl));

            if (ret > 0)
            {
                printf("%s",stringl);
                printf("\n\r\n\r");

                int interfaceNum = 0;
                if(desc.iProduct == 2)
                {

                    if(libusb_kernel_driver_active(handle, interfaceNum) == 1)
                    {
                        printf("\nKernel Driver Active\n");
                        if(libusb_detach_kernel_driver(handle, interfaceNum) == 0)
                            printf("\nKernel Driver Detached!\n");
                        else
                        {
                            printf("\nCouldn't detach kernel driver!\n");
                            libusb_free_device_list(devs, 2);
                            libusb_close(handle);
                        }
                    }

                    int interfaceClaimed = libusb_claim_interface(handle, interfaceNum);
                    if(interfaceClaimed < 0)
                    {
                        printf("\nCannot Claim Interface\n");
                        libusb_free_device_list(devs, 1);
                        libusb_close(handle);
                    }
                    else
                        printf("\nClaimed Interface\n");

                    unsigned char data = 'p';
                    unsigned char *dataPointer = &data;

                    int numDataTransferred=0;
                    int *numDataTransferredPointer = &numDataTransferred;

                    printf("initiating bulk transfer...\n");
                    int bulk_trans_failed = libusb_bulk_transfer(handle,0x81,dataPointer,1,numDataTransferredPointer,1000);

                    if(bulk_trans_failed)
                    {
                        printf("bulk Transfer failed, returned %d aka %s\n",bulk_trans_failed, libusb_error_name(bulk_trans_failed));
                    }
                    else
                    {
                        printf("bulk Transfer suceeded\n");
                    }
                    printf("numDataTransferred=%d\n",numDataTransferred);
                }
            }
        }
    }

    libusb_free_device_list(devs, 2);
    libusb_exit(NULL);
    return 0;
}










