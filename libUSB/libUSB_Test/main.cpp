#include <libusb.h>
#include <stdio.h>
#include <stdlib.h>



int main(void)
{
    libusb_context* context = NULL;//using null becouse of pointer


    int kernelDriverDetached = 0;  /* Set to 1 if kernel driver detached*/
    uint8_t bufferOUT[128];                /* 64 byte transfer buffer */
    uint8_t bufferIN[128];                /* 64 byte transfer buffer */

    int numBytesTransferred = 0;  /* Actual bytes transferred. */

    libusb_device_handle* handle = NULL;
    int res = libusb_init(&context); //initialize the library using libusb_init


    if (res != 0)
    {
      fprintf(stderr, "Error initialising libusb.\n");

    }


    /* Get the first device with the matching Vendor ID and Product ID.If
    * intending to allow multiple demo boards to be connected at once,you
    * will need to use libusb_get_device_list() instead. Refer to the libusb
    * documentation for details. */
    handle = libusb_open_device_with_vid_pid(context, 0x1cbe, 0x0003);

    if (!handle)
    {
        fprintf(stderr, "Unable to open device.\n");

    }

    /* Check whether a kernel driver is attached to interface #0. If so, we'll
      * need to detach it.*/
     if (libusb_kernel_driver_active(handle, 0))
     {
         //attempt to detach
        res = libusb_detach_kernel_driver(handle, 0);
        if (res == 0)
        {
           kernelDriverDetached = 1;
        }
        else
        {
           fprintf(stderr, "Error detaching kernel driver. %s\n", libusb_error_name(res));
           kernelDriverDetached = 0;
        }
    }

       /* Claim interface #0. */
    res = libusb_claim_interface(handle, 0);//bInterfaceNumber =0
    if (res != 0)
    {
       fprintf(stderr, "Error claiming interface.\n");

    }

    memset(bufferOUT, 0, 128);
    bufferOUT[0] = 0x01;//0
    bufferOUT[1] = 0x01;//1
    bufferOUT[2] = 0x02;//2
    bufferOUT[3] = 0x7D;//125
    bufferOUT[4] = 0x7E;//126
    bufferOUT[5] = 0x7F;//127
    bufferOUT[6] = 0xFA;//250
    bufferOUT[7] = 0xFB;//251
    bufferOUT[8] = 0xFC;//252
    bufferOUT[9] = 0xFD;//253
    bufferOUT[10] = 0xFE;//254
    bufferOUT[11] = 0xFF;//255
    bufferOUT[127] = 0x0A;

    res = libusb_bulk_transfer(handle, 0x01, bufferOUT, 1, &numBytesTransferred, 10000);
    if (res == 0)
    {
       printf("%d bytes transmitted successfully.\n", numBytesTransferred);
    }
    else
    {
      fprintf(stderr, "Error during send message: %s\n",libusb_error_name(res));
    }

    system("pause");
    memset(bufferIN, 0, 1);

     res = libusb_bulk_transfer(handle, 0x81, bufferIN, 1, &numBytesTransferred, 10000);
     if (res == 0)
     {
        printf("%d bytes received successfully.\n", numBytesTransferred);

        for(int bufferINLoop=0; bufferINLoop<=0; bufferINLoop++)
        {
            printf("bufferIN[%d]=%X\n",bufferINLoop,bufferIN[bufferINLoop]);
        }
     }
     else
     {
        fprintf(stderr, "Error during receibe response:%s\n",libusb_error_name(res));
     }

    /* Release interface #0. */
    res = libusb_release_interface(handle, 0);
    if (0 != res)
    {
       fprintf(stderr, "Error releasing interface.\n");
    }

    /* Shutdown libusb. */
    libusb_exit(0);
    system("pause");
    return 0;
    }
